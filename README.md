# librephotos-images

This is an attempt to build the images from LibrePhotos and put them in a container registry.
Source: https://github.com/LibrePhotos/librephotos

For better or worse, this takes everything from the `main` source branch and pushes it to the `latest` tag.

## Backend
```bash
docker pull registry.gitlab.com/rveach/librephotos-images/backend:latest
```

## Frontend
```bash
docker pull registry.gitlab.com/rveach/librephotos-images/frontend:latest
```

## proxy
```bash
docker pull registry.gitlab.com/rveach/librephotos-images/proxy:latest
```